/*
 * Copyright © 2020 Property of Rían Errity Licensed under GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007. See <LICENSE.md>
 */

package co.paradaux.hibernia.VoidSpawn.events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

public class PlayerMoveEventListener implements Listener {

    Plugin plugin;
    String order;
    Boolean worldspawn;

    public PlayerMoveEventListener(Plugin plugin) {
        this.plugin = plugin;
        this.order = plugin.getConfig().getString("fall-cmd");
        assert order != null;
        if (order.equals("worldspawn")) {
            worldspawn = true;
        }
        this.worldspawn = false;
    }

    @EventHandler
    public void onFall(PlayerMoveEvent e) {
        if (e.getPlayer().getLocation().getY() <= -64.00) {
            if (worldspawn) {
                e.getPlayer().teleport(e.getPlayer().getWorld().getSpawnLocation());
            }
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), order.replace("%name%", e.getPlayer().getName()));
        }
    }

}
